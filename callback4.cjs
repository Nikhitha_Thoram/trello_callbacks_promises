const getLists = require("./callback2.cjs");
const getBoardId = require("./callback1.cjs");
const getCards = require("./callback3.cjs");

function getAllDetails() {
  getBoardId("mcu453ed")
    .then((board) => {
      console.log(board);
    })
    .catch((error) => {
      console.error(error.message);
    });

  getLists("mcu453ed")
    .then((lists) => {
      console.log(lists);
    })
    .catch((error) => {
      console.error(error.message);
    });

  getCards("qwsa221")
    .then((cards) => {
      console.log(cards);
    })
    .catch((error) => {
      console.error(error.message);
    });
}

module.exports = getAllDetails;
