const lists = require("./lists_1.json");
function getLists(boardId) {
  return new Promise((resolve, reject) => {
    if (boardId && typeof boardId !== "string") {
      reject(new Error("boardId is not a string"));
      return;
    }
    const foundLists = lists[boardId];
    if (foundLists) {
      resolve(foundLists);
    } else {
      reject(new Error("ListId is not found"));
    }
  });
}
module.exports = getLists;
