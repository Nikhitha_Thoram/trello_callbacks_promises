const getBoardId = require("./callback1.cjs");
const getLists = require("./callback2.cjs");
const getCards = require("./callback3.cjs");

function getDetailsOfLists() {
  getBoardId("mcu453ed")
    .then((board) => {
      console.log(board);
    })
    .catch((error) => {
      console.error(error.message);
    });

  getLists("mcu453ed")
    .then((lists) => {
      console.log(lists);
    })
    .catch((error) => {
      console.error(error.message);
    });

  getCards("qwsa221")
    .then((cards) => {
      console.log(cards);
    })
    .catch((error) => {
      console.error(error);
    });

  getCards("jwkh245")
    .then((cards) => {
      console.log(cards);
    })
    .catch((error) => {
      console.error(error);
    });
}

module.exports = getDetailsOfLists;
