const cards = require("./cards_1.json");

function getCards(listId) {
  return new Promise((resolve, reject) => {
    if (listId && typeof listId !== "string") {
      reject(new Error("listId is not a string"));
      return;
    }
    const foundCards = cards[listId];
    if (foundCards) {
      resolve(foundCards);
    } else {
      reject(new Error("ListId is not found"));
    }
  });
}
module.exports = getCards;
