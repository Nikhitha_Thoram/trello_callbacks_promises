const getBoardId = require("./callback1.cjs");
const getLists = require("./callback2.cjs");
const getCards = require("./callback3.cjs");
const lists = require("./lists_1.json");

function getDetailsOfListsAndCards() {
  getBoardId("mcu453ed")
    .then((board) => {
      console.log(board);
    })
    .catch((error) => {
      console.error(error.message);
    });

  getLists("mcu453ed")
    .then((lists) => {
      console.log(lists);
    })
    .catch((error) => {
      console.error(error.message);
    });

    const listKeys = Object.keys(lists);
    for(let key of listKeys){
        const list = lists[key];
        for(let object of list){
            getCards(object.id)
            .then((cards)=>{
                console.log(cards);
            })
            .catch((error)=>{
                console.error(error);
            })
        }
    }
}
module.exports = getDetailsOfListsAndCards;
