const boards = require("./boards_1.json");

function getBoardId(boardId) {
  return new Promise((resolve, reject) => {
    if (boardId && typeof boardId !== "string") {
      reject(new Error("Board Id is not a string"));
      return;
    }
    const board = boards.find((board) => board.id === boardId);
    if (board) {
      resolve(board);
    } else {
      reject(new Error("No boardId found"));
    }
  });
}

module.exports = getBoardId;
