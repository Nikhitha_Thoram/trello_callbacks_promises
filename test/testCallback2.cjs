const getLists = require("../callback2.cjs");

getLists("abc122dc")
  .then((lists) => {
    console.log(lists);
})
  .catch((error) => {
    console.error(error.message);
});
